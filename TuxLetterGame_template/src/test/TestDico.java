/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.io.*;

import game.Dico;
import org.xml.sax.SAXException;

/**
 *
 * @author Vafing
 */
public class TestDico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SAXException, IOException {
       Dico dico = new Dico("Chemin");
       
       dico.lireDictionnaireDOM("src/xml", "dico.xml");
       System.out.println(dico.getMotDepuisListeNiveaux(2));
    }
    
}