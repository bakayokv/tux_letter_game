/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xml.XMLUtil;

/**
 *
 * @author Vafing
 */
public class EditeurDico {
    private Document editeurDico, doc;
    private String fichier = "src/testediteurdico/dico.xml";

    public EditeurDico() throws SAXException, IOException {
        this.editeurDico = editeurDico;
    }

    public Document getEditeurDico(){
        return editeurDico;
    }

    public void setEditeurDico(Document editeurDico) {
        this.editeurDico = editeurDico;
    }
    
    public void lireDom(String fichier) throws SAXException, IOException{
        DOMParser parser = new DOMParser();
        parser.parse(fichier);
        this.editeurDico = parser.getDocument();
    }
    
    public void ecrireDom(String fichier) throws Exception
    {
        XMLUtil.DocumentTransform.writeDoc(this.editeurDico, fichier);
    }    
    public void ajouteMot(String mot, int niveau) throws SAXException, IOException, Exception{
        this.lireDom(fichier);
        
        
        // getChildNodes renvoie la liste des noeuds fils du noeud courant
        NodeList nodes = editeurDico.getElementsByTagName("tux:dictionnaire");
        NodeList nodesaa = editeurDico.getElementsByTagName("tux:mot");
        Element new_word;    
            //System.out.println(nodes.getLength()+"<=");
            //System.out.println(n.getChildNodes().item(0).getTextContent());
            //System.out.println(n.getChildNodes().item(0).getTextContent().length());
          
            
            switch (niveau) {
                
                case 1:
                    new_word = editeurDico.createElement("tux:mot");
                        new_word.setAttribute("niveau",""+niveau);
                        new_word.setTextContent(mot);
                        nodes.item(0).appendChild(new_word);
                        this.ecrireDom(fichier);
                    break;
                case 2:
                    new_word = editeurDico.createElement("tux:mot");
                        new_word.setAttribute("niveau",""+niveau);
                        new_word.setTextContent(mot);
                        nodes.item(0).appendChild(new_word);
                        this.ecrireDom(fichier);
                    break;
                case 3:
                    new_word = editeurDico.createElement("tux:mot");
                        new_word.setAttribute("niveau",""+niveau);
                        new_word.setTextContent(mot);
                        nodes.item(0).appendChild(new_word);
                        this.ecrireDom(fichier);
                    break;
                case 4:
                    new_word = editeurDico.createElement("tux:mot");
                        new_word.setAttribute("niveau",""+niveau);
                        new_word.setTextContent(mot);
                        nodes.item(0).appendChild(new_word);
                        this.ecrireDom(fichier);
                    break;
                default:
                    new_word = editeurDico.createElement("tux:mot");
                        new_word.setAttribute("niveau",""+niveau);
                        new_word.setTextContent(mot);
                        nodes.item(0).appendChild(new_word);
                        this.ecrireDom(fichier);
                    break;
            }
    }
}
