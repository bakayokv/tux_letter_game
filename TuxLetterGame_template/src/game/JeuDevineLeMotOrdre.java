/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import static com.sun.javafx.animation.TickCalculation.add;
import java.util.ArrayList;
import static javafx.application.Platform.exit;


/**
 *
 * @author Vafing
 */
public class JeuDevineLeMotOrdre extends Jeu{
    private int nbLettresRestantes;
    private Chronometre chrono;
    private ArrayList<Letter> lettres;
    int a;
    String s = "";
    EnvText endGame;
    
    public JeuDevineLeMotOrdre() {
        super();
        chrono = new Chronometre(10);
    }
    
    boolean tuxTrouveLettre() {
        char c;
        // affectation de la liste de lettres
        this.lettres = this.getLettres();
        if (!lettres.isEmpty()) {
            Letter letter = lettres.get(0);
            System.out.println("Lettres"+letter);
            if (collision(letter)) {
                c = lettres.get(0).getLetter();               
                //System.out.println("Lettres :"+ c);
                s = s + c;     
                super.retirerLettreTrouve(lettres.get(0));
                lettres.remove(0);
                //System.out.println("s :" + s);
                return true;
            }
        }
        return false;
    }
    
    
    private int getNbLettresRestantes(){
       return nbLettresRestantes; 
    }
    
    private int getTemps(){
       return this.chrono.getSeconds(); 
    }

    @Override
    protected void démarrePartie(Partie partie) {    
        
        chrono.start();
        String mot = partie.getMot();
        System.out.println("longueur mot : "+ mot.length());
        this.nbLettresRestantes = mot.length();
        partie.setTrouve(0);
        this.a = 0;
        
    }
    
    @Override
    protected void appliqueRegles(Partie partie) {
        
        if(chrono.remainsTime()){
            chrono.stop();
        }else if(!chrono.remainsTime()){
            terminePartie(partie);
        }
        else if(nbLettresRestantes == 0){
            this.terminePartie(partie);
            
        }
        if(tuxTrouveLettre()) {
            a++;
            nbLettresRestantes--;
            partie.setTrouve(a);
            partie.setMotTrouve(s);
        }
             
        //System.out.println("a:" +a);
        //System.out.println("nb lettres restantes:"+this.nbLettresRestantes);
        System.out.println("temps:"+chrono.getSeconds());
        System.out.println("begin : " + chrono.getBegin() +"\n"  + "end :" + chrono.getEnd());


    }

    @Override
    protected void terminePartie(Partie partie) {
        chrono.stop();
        partie.setTemps(chrono.getSeconds());
        partie.setTrouve(nbLettresRestantes);
        System.out.println("string : " + s);
        super.template(s);
    }
    
}
