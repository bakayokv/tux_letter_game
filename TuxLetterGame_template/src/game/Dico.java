/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
/**
 *
 * @author Vafing
 */
public class Dico extends  DefaultHandler{
    private ArrayList<String> listeNiveau1;
    private ArrayList<String> listeNiveau2;
    private ArrayList<String> listeNiveau3;
    private ArrayList<String> listeNiveau4;
    private ArrayList<String> listeNiveau5;
    private ArrayList<String> dico;
    private String attributes;
    private String mot;
    
    private String cheminFichierDico;
    
    private StringBuffer buffer;
    boolean tuxDictionnaire, tuxmot, tuxmot_niv1, tuxmot_niv2, tuxmot_niv3, tuxmot_niv4, tuxmot_niv5;

    public Dico() {
        super();
    }
    
    public Dico(String cheminFichierDico) {
        this.cheminFichierDico = cheminFichierDico;
        listeNiveau1 = new ArrayList<>();
        listeNiveau2 = new ArrayList<>();
        listeNiveau3 = new ArrayList<>();
        listeNiveau4 = new ArrayList<>();
        listeNiveau5 = new ArrayList<>();

    }
    
    public String getMotDepuisListeNiveaux(int niveau){
        
        switch(verifieNiveau(niveau)){
            case 5: 
                return getMotDepuisListe(listeNiveau5);
            case 4:
                return getMotDepuisListe(listeNiveau4);
            case 3:
                return getMotDepuisListe(listeNiveau3);
            case 2:
                return getMotDepuisListe(listeNiveau2);
            default:
                return getMotDepuisListe(listeNiveau1);
        }
    }

    public void ajouteMotADico(int niveau, String mot){
        switch(verifieNiveau(niveau)){
            case 5: 
                listeNiveau5.add(mot);
                break;
            case 4:
                listeNiveau4.add(mot);
                break;
            case 3:
                listeNiveau3.add(mot);
                break;
            case 2:
                listeNiveau2.add(mot);
                break;
            default:
                listeNiveau1.add(mot);
                break;
        }
    }
    
    public void ajouteMotDico(String mot){
        switch(mot.length()){
            case 3 :
                this.ajouteMotADico(1, mot);
                break;
            case 4 :
                this.ajouteMotADico(2, mot);
                break;
            case 5 :
                this.ajouteMotADico(3, mot);
                break;
            case 6:
                this.ajouteMotADico(4, mot);
                break;
            default : 
                this.ajouteMotADico(5, mot);
                break;
        }
        
    }

    public String getCheminFichierDico() {
        return cheminFichierDico;
    }

   
    public int verifieNiveau( int niveau){
        int newNiv =0;
        if(niveau < 1 || niveau > 5){
            newNiv = 1;
        }else{
            newNiv = niveau;
        }
        return newNiv;
    }
    
    
    public String getMotDepuisListe(ArrayList<String> list){
        int Min = 0;
        int Max = list.size()-1;
        int nombreAleatoire = Min + (int)(Math.random() * ((Max - Min) + 1));
        if(list.size()>0){
            return list.get(nombreAleatoire);
        }
        return null;
    }
    
    public void lireDictionnaireDOM(String path,String filename) throws SAXException, IOException{
        
        DOMParser parser = new DOMParser() ;
        parser.parse(path+"/"+filename);
        Document doc = parser.getDocument();
        
        // ne pas compter les espaces comme des noeuds
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringElementContentWhitespace(true);
        
        // getChildNodes renvoie la liste des noeuds fils du noeud courant
        NodeList nodes = doc.getElementsByTagName("tux:mot");
        for(int i=0; i<= nodes.getLength()-1; i++){
            Node n = nodes.item(i);
            //System.out.println(n);
            //System.out.println(n.getChildNodes().item(0).getTextContent());
            //System.out.println(n.getChildNodes().item(0).getTextContent().length());
            
            switch (n.getChildNodes().item(0).getTextContent().length()) {
                
                case 3:
                    this.ajouteMotADico(1, n.getChildNodes().item(0).getTextContent());
                    break;
                case 4:
                    this.ajouteMotADico(2, n.getChildNodes().item(0).getTextContent());
                    break;
                case 5:
                    this.ajouteMotADico(3,n.getChildNodes().item(0).getTextContent());
                    break;
                case 6:
                    this.ajouteMotADico(4,n.getChildNodes().item(0).getTextContent());
                    break;
                default:
                    this.ajouteMotADico(5,n.getChildNodes().item(0).getTextContent());
                    break;
            }
        }
    }
    
    public void lireDictionnaire() throws IOException{
    
    try {
            SAXParserFactory fabrique = SAXParserFactory.newInstance();
            SAXParser parseur = fabrique.newSAXParser();;
             
            File fichier = new File("src/XML/dico.xml");
            DefaultHandler gestionnaire = this;
            parseur.parse(fichier, gestionnaire);
            
        } catch (SAXException | ParserConfigurationException ex) {
            Logger.getLogger(Dico.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }

    @Override
    public void characters(char[] chars, int start, int length) throws SAXException {
        if (this.tuxmot) {
            for (int j = start  ; j < start + length ; j ++) {
             this.mot = this.mot+""+chars[j];
            }
            this.ajouteMotDico(this.mot);
            //System.out.println(mot.length());
        }   
    }

    @Override
    public void endElement(String string, String string1, String string2) throws SAXException {
        if(string2.equals("tux:dictionnaire")){ 
            this.tuxDictionnaire = false; 
	}else if(string2.equals("tux:mot")){
            this.tuxmot = false;
            this.mot = "";
        }else{ 
            //erreur, on peut lever une exception 
            throw new SAXException("Balise "+string2+" inconnue."); 
	}    
    }
                                        
    @Override
    public void startElement(String string, String string1, String string2, Attributes atrbts) throws SAXException {
        if(string2.equals("tux:dictionnaire")){
            System.out.println("Debut du document existe");
            this.tuxDictionnaire = true;
            //System.out.println(this.tuxDictionnaire);
        }else if(string2.equals("tux:mot")){
                this.tuxmot = true; 
                this.tuxmot_niv1 = true;
                this.attributes = atrbts.getValue("niveau");
                this.mot = "";    
                //System.out.println("mot: "+this.mot);
                
                /*}else if(Integer.parseInt(atrbts.getValue("niveau")) == 2){
                    this.tuxmot_niv2 = true;
                     this.attributes = atrbts.getValue("niveau");
                    System.out.println("niv 2 : "+this.tuxmot_niv2);
                }else if(Integer.parseInt(atrbts.getValue("niveau")) == 3){
                    this.tuxmot_niv3 = true;
                    this.attributes = atrbts.getValue("niveau");
                    System.out.println("niv 3 : "+this.tuxmot_niv3);
                }else if(Integer.parseInt(atrbts.getValue("niveau")) == 4){
                    this.tuxmot_niv4 = true;
                    this.attributes = atrbts.getValue("niveau");
                    System.out.println("niv 4 : "+this.tuxmot_niv4);
                }else if(Integer.parseInt(atrbts.getValue("niveau")) == 5){
                    this.tuxmot_niv5 = true;
                    this.attributes = atrbts.getValue("niveau");
                    System.out.println("niv 5 : "+this.tuxmot_niv5 );
                }
                */
             
        }else{
            throw new SAXException("Balise"+string2+"inconnue");
        }
    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("Fin du parsing"); 
        //System.out.println("niv : "+this.listeNiveau1.get(2) );

    }

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Début du parsing");
    }

    
    
            
}
