/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import static com.jme3.scene.VertexBuffer.Format.Int;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xml.XMLUtil;

/**
 *
 * @author Vafing
 */
public class Profil {
    
    private String nom;
    private String nomVerif;
    private String avatar;//="avatar.pjg";
    private String dateNaissance;//="dd.MMMMM.yyyy";
    private ArrayList<Partie> parties;
    public Document _doc;

    public Profil(String nom, String dateNaissance) {
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        parties = new ArrayList<Partie>();

    }
    public Profil(){
        
    }
    
    public Profil(String filename) throws SAXException, IOException {
        
        File f = new File(filename);
    if(f.exists()){
        _doc = fromXML(filename);
        
        // ne pas compter les espaces comme des noeuds
        NodeList nodes = _doc.getElementsByTagName("ns1:profil");
        System.out.println(nodes.getLength());
            Node n = nodes.item(0);
            
                nom = n.getChildNodes().item(1).getTextContent();
                avatar = n.getChildNodes().item(3).getTextContent();
                dateNaissance = n.getChildNodes().item(5).getTextContent();
                 //System.out.println(nom + "\n" + avatar + "\n" + dateNaissance);
            
            //pour les parties existantes
            // verification du nom 
            
                parties = new ArrayList<Partie>();
                NodeList party = _doc.getElementsByTagName("ns1:partie");
                Element partieE;
                Partie p;
            
                for(int i = 0; i< party.getLength(); i++){
                    partieE = (Element) party.item(i);
                    //System.out.println(partieE.getTextContent());
                    p = new Partie(partieE);
                    parties.add(p);
                   //System.out.println(parties.get(i));
                }     
    } else {
        System.out.println("Erreur nom profil");
    }    
            
    }
      
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public ArrayList<Partie> getParties() {
        return parties;
    }

    public void setParties(ArrayList<Partie> parties) {
        this.parties = parties;
    }
    

    boolean charge(String nomJoueur) {
        Document doc = this.fromXML("src/XML/profil.xml");
        NodeList nom = doc.getElementsByTagName("ns1:nom");
        NodeList avatar = doc.getElementsByTagName("ns1:avatar");
        NodeList date = doc.getElementsByTagName("ns1:anniversaire");
        Node n = null;
        int place = 0;

        int i, j;
        System.out.println(nom.getLength());
        for(i = 0; i < nom.getLength(); i++){
                n = nom.item(i);
                //if(nomJoueur == n.getTextContent())
                if(nomJoueur == null ? n.getTextContent() == null : nomJoueur.equals(n.getTextContent())){
                    place = i;
                    System.out.println(place);
                }
            }
        
        
        //traitement du nom
        this.nom = nom.item(place).getTextContent();
        
        for(j = 0; j<nom.item(place).getTextContent().length();j++){
            if(nom.item(place).getTextContent().charAt(j) != nomJoueur.charAt(j)){
                return false;
            }
        }
        this.avatar = avatar.item(place).getTextContent();
        this.dateNaissance = date.item(place).getTextContent();
        System.out.println(this.nom + " " + this.avatar);
        /*
        NodeList parties = doc.getElementsByTagName("partie");
            parties.getLength();
            //System.out.println(parties.getLength());
            for(i=0; i < parties.getLength(); i++){
                Element partie = (Element) doc.getElementsByTagName("parties").item(i);  
                Element temps;
                if(partie.getElementsByTagName("temps").getLength() == 0){
                    temps = null;
                }else{
                temps = (Element) partie.getElementsByTagName("temps").item(0);
                                System.out.println(temps.getTextContent());
                }
                Element mot = (Element) partie.getElementsByTagName("mot").item(0);
                                System.out.println(mot.getTextContent());
                
                Partie p;
                p = new Partie(partie);
                System.out.println(p);
                //this.parties.add(p);
            }
        */
        return true;
    }
    
     // Cree un DOM à partir d'un fichier XML
    public Document fromXML(String nomFichier) {
        try {
            return XMLUtil.DocumentFactory.fromFile(nomFichier);
        } catch (Exception ex) {
            Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void toXML(String nomFichier) {
        try {
            XMLUtil.DocumentTransform.writeDoc(_doc, nomFichier);
        } catch (Exception ex) {
            Logger.getLogger(Profil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    /// Takes a date in XML format (i.e. ????-??-??) and returns a date
    /// in profile format: dd/mm/yyyy
    public static String xmlDateToProfileDate(String xmlDate) {
        String date;
        // récupérer le jour
        date = xmlDate.substring(xmlDate.lastIndexOf("-") + 1, xmlDate.length());
        date += "/";
        // récupérer le mois
        date += xmlDate.substring(xmlDate.indexOf("-") + 1, xmlDate.lastIndexOf("-"));
        date += "/";
        // récupérer l'année
        date += xmlDate.substring(0, xmlDate.indexOf("-"));

        return date;
    }
    
    /// Takes a date in profile format: dd/mm/yyyy and returns a date
    /// in XML format (i.e. ????-??-??)
    public static String profileDateToXmlDate(String profileDate) {
        String date;
        // Récupérer l'année
        date = profileDate.substring(profileDate.lastIndexOf("/") + 1, profileDate.length());
        date += "-";
        // Récupérer  le mois
        date += profileDate.substring(profileDate.indexOf("/") + 1, profileDate.lastIndexOf("/"));
        date += "-";
        // Récupérer le jour
        date += profileDate.substring(0, profileDate.indexOf("/"));

        return date;
    }
    
    
    public void ajoutePartie(Partie p){
        this.parties.add(p);
    }
     
    //enregistreProfil  attention a faire
    public void sauvegarder( String filename, Partie partie){
        try {
            Document doc = XMLUtil.DocumentFactory.fromFile(filename);
            NodeList profil = doc.getElementsByTagName("ns1:profil");
            NodeList nom = doc.getElementsByTagName("ns1:nom");
            Element parties = doc.createElement("ns1:parties");
            
            System.out.println("Debut de la sauvegarder ...");
            NodeList compteurNom = doc.getElementsByTagName("ns1:nom");
            int cn = 0;
            System.out.println(compteurNom.getLength());
            
            for(int i =0 ; i< compteurNom.getLength(); i++){
                if(nom.item(i).getTextContent().equals(this.nom)){
                    cn = i;       
                }
            }
            System.out.println(cn);
            if(cn != 0){ // si le nom est deja present
                        //parties = doc.getElementsByTagName("parties");
                System.out.println("Sauvegarde dans l'une des parties de " + this.nom);
                Node ancien_partie = doc.getElementsByTagName("ns1:parties").item(cn);
                Element part = doc.createElement("ns1:partie");
                        part.setAttribute("date", partie.getDate());
                        part.setAttribute("trouve", partie.getMotTrouve());
                        
                Element temps = doc.createElement("ns1:temps");
                        temps.setTextContent(""+partie.getTemps());
                        
                Element mot = doc.createElement("ns1:mot");
                        mot.setAttribute("niveau", ""+partie.getNiveau());
                        mot.setTextContent(partie.getMot());
                        
                part.appendChild(temps);
                part.appendChild(mot);
                
                ancien_partie.appendChild(part);
                profil.item(0).appendChild(parties);
                        
            }else{// si on doit creer un nouveau champs champ nom et autre
                System.out.println("Sauvegarde une nouvelle partie au  nom de " + this.nom);
                Element nomElt = doc.createElement("ns1:nom");
                        nomElt.setTextContent(this.nom);
                        profil.item(0).appendChild(nomElt);
                        
                Element avatar = doc.createElement("ns1:avatar");
                        avatar.setTextContent(this.avatar);
                        profil.item(0).appendChild(avatar);
                        
                Element anniversaire = doc.createElement("ns1:anniversaire");
                        anniversaire.setTextContent(profileDateToXmlDate(this.dateNaissance));
                        profil.item(0).appendChild(anniversaire);
                
                Element part = doc.createElement("ns1:partie");
                        part.setAttribute("date", partie.getDate());
                        part.setAttribute("trouve", partie.getMotTrouve());
                        
                Element temps = doc.createElement("ns1:temps");
                        temps.setTextContent(""+partie.getTemps());
                        
                Element mot = doc.createElement("ns1:mot");
                        mot.setAttribute("niveau", ""+partie.getNiveau());
                        mot.setTextContent(partie.getMot());
                
                part.appendChild(temps);
                part.appendChild(mot);
                
                parties.appendChild(part);
                profil.item(0).appendChild(parties);
                
                /*
                Element partie = doc.createElement("partie");
                        partie.setAttribute("date", "apres aujourd'hui");
                        parties.appendChild(partie);
                        
                Element temps = doc.createElement("temps");
                        temps.setTextContent(temps.toString());
                        partie.appendChild(temps);
                        
                Element mot = doc.createElement("mot");
                        mot.setTextContent(p.getMot());
                        mot.setAttribute("niveau", p.getNiveau() + "");
                        partie.appendChild(mot);
               */
            }
        XMLUtil.DocumentTransform.writeDoc(doc, filename);  
        } catch (Exception ex) {
            Logger.getLogger(Partie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    // a implementer 

    public boolean compareLettre(String s, String v){
        int j = 0;
        for(int i = 0; i< s.length(); i++){
            if(s.charAt(i) != v.charAt(i)){
               return false;
            }
        }
        return true;
    }
}
