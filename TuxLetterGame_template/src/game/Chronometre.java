package game;

public class Chronometre {
    private long begin;
    private long end;
    private long current;
    private int limite;

    public Chronometre(int limite) {
        //intialisation
        this.limite = limite;
    }
    
    public void start(){
        begin = System.currentTimeMillis();
    }

    public void setLimite(int limite) {
        this.limite = limite;
    }
 
    public void stop(){
        end = System.currentTimeMillis();
    }
 
    public long getTime() {
        return end-begin;
    }
 
    public long getMilliseconds() {
        return end-begin;
    }
 
    public int getSeconds() {
        return (int) ((end - begin) / 1000.0);
    }
 
    public double getMinutes() {
        return (end - begin) / 60000.0;
    }
 
    public double getHours() {
        return (end - begin) / 3600000.0;
    }

    public long getBegin() {
        return begin;
    }

    public long getEnd() {
        return end;
    }
    
    
    /**
    * Method to know if it remains time.
    */
    public boolean remainsTime() {
        current = System.currentTimeMillis();
        int timeSpent;
        timeSpent = this.getSeconds();
        return timeSpent < this.limite;
    }
    
    public int tempsSeconde(){
        current = System.currentTimeMillis();
        return  (int) ((current - begin)/1000.0);
    }
     
}
