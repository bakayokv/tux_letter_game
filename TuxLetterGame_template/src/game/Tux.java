/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import env3d.Env;
import env3d.advanced.EnvNode;
import org.lwjgl.input.Keyboard;

/**
 *
 * @author Vafing
 */
public class Tux extends EnvNode{
    
    private Env env;
    private Room room;
    
    public Tux(Env env, Room room) {
        //this.env = new Env();
        this.env=env;
        this.room = room;
        setScale(4.0);
        setX(room.getWidth()/2);// positionnement au milieu de la largeur de la room
        setY(getScale() * 1.1); // positionnement en hauteur basé sur la taille de Tux
        setZ(room.getDepth()/2); // positionnement au milieu de la profondeur de la room
        setTexture("models/tux/tux.png");
        setModel("models/tux/tux.obj");   
    }
   
    public void deplace(){
        if (env.getKeyDown(Keyboard.KEY_Z) || env.getKeyDown(Keyboard.KEY_UP)) { // Fleche 'haut' ou Z
       // Haut
            if(testeRoomCollision(this.getZ(),this.getScale())){
                this.setRotateY(180);
                this.setZ(this.getZ() - 1.0);
            }
            
       }
       if (env.getKeyDown(Keyboard.KEY_Q) || env.getKeyDown(Keyboard.KEY_LEFT)) { // Fleche 'gauche' ou Q
       // Gauche
            if(testeRoomCollision(this.getX(),this.getScale())){
                this.setRotateY(-90);
                this.setX(this.getX() - 1.0);
            }
       }
    
       if (env.getKeyDown(Keyboard.KEY_D) || env.getKeyDown(Keyboard.KEY_DOWN)){ // Fleche 'bas' ou D
       // Bas
            if(!testeRoomCollision(this.getZ(),room.getDepth()-this.getScale())){
                this.setRotateY(0);
                this.setZ(this.getZ() + 1.0);
            }
        
       }
    
       if (env.getKeyDown(Keyboard.KEY_R) || env.getKeyDown(Keyboard.KEY_RIGHT)) { // Fleche 'droite' ou R
       // Droite
            if(!testeRoomCollision(this.getX(),room.getWidth()-this.getScale())){
                this.setRotateY(90);
                this.setX(this.getX() + 1.0);
            }
       }
    }
    
    // code pris chez un eleve qui m'a aidé John Fomba
    private Boolean testeRoomCollision(double x ,double z){
        Boolean resultat = false; 
        if(x>z){
            resultat = true;
        }
        return resultat;
    }
}
