/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import env3d.advanced.EnvNode;
import java.util.ArrayList;
import game.Letter;

/**
 *
 * @author Vafing
 */
public class Letter extends EnvNode{
    private char letter;
    private Room room;
    
    public Letter(char l, double x, double z){
        this.letter = l;
       /*
        setX(x);// positionnement au milieu de la largeur de la room
        setY(getScale() * 3.1); // positionnement en hauteur basé sur la taille de Tux
       //setY(100); 
        setZ(z); // positionnement au milieu de la profondeur de la room
        */
        this.letter = l;
       
        this.room = room;
            String png="cube.png";
            if(l !=' '){
                png=l+".png";
            }
        
            setScale(2.0);
            setX(x*8.1);
            setY(getScale());            
            setZ(z * 8.1);
            setTexture("models/letter/"+png);
            setModel("models/cube/cube.obj");
    }

    
     public char getLetter() {
        return letter;
    }
    
}
