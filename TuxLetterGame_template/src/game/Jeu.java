/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import env3d.Env;
import java.util.ArrayList;
import org.lwjgl.input.Keyboard;
import env3d.Env;
import static game.Profil.profileDateToXmlDate;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.SAXException;


/**
 *
 * @author gladen
 */
public abstract class Jeu {

    // Une énumération pour définir les choix de l'utilisateur
    public enum MENU_VAL {
        MENU_SORTIE, MENU_CONTINUE, MENU_JOUE;
    }

    private final Room menuRoom;
    EnvText textNomJoueur;
    EnvText textMenuQuestion;
    EnvText textMenuJeu1;
    EnvText textMenuJeu2;
    EnvText textMenuJeu3;
    EnvText textMenuJeu4;
    EnvText textMenuPrincipal1;
    EnvText textMenuPrincipal2;
    EnvText textMenuPrincipal3;
    EnvText textNiveauU;
    EnvText textNiveau1;
    EnvText textNiveau2;
    EnvText textNiveau3;
    EnvText textNiveau4;
    EnvText textNiveau5;
    EnvText text;
    EnvText datation;
    EnvText finGame;
    EnvText finGameTemps;

    Env env;
    private Room room;
    private Plateau plateau;
    private Profil profil;
    private ArrayList<Letter> lettres;
    private Dico dico;
    private Tux tux;
    private Chronometre chrono;




    public Jeu() {
        
        Room mainRoom = new Room();
        // Instancie une autre Room pour les menus
        menuRoom = new Room();
        menuRoom.setTextureEast("textures/black.png");
        menuRoom.setTextureWest("textures/black.png");
        menuRoom.setTextureNorth("textures/black.png");
        menuRoom.setTextureBottom("textures/black.png");
        
        // Crée un nouvel environnement
        env = new Env();
 
        // Instancie une Room
        room = new Room();
        // Instancie un tux;
    
        // Règle la camera
        env.setCameraXYZ(50, 60, 175);
        env.setCameraPitch(-20);
 
        // Désactive les contrôles par défaut
        env.setDefaultControl(false);
 
        // Instancie un profil par défaut
        profil = new Profil();
        chrono = new Chronometre(60);
        // Instancie le conteneur de lettres
        this.lettres = new ArrayList<Letter>();
        this.dico = new Dico("ah/ahj");

        // Textes affichés à l'écran
        textMenuQuestion = new EnvText(env, "Voulez vous ?", 200, 300);
        textMenuJeu1 = new EnvText(env, "1. Commencer une nouvelle partie ?", 250, 280);
        textMenuJeu2 = new EnvText(env, "2. Charger une partie existante ?", 250, 260);
        textMenuJeu3 = new EnvText(env, "3. Sortir de ce jeu ?", 250, 240);
        textMenuJeu4 = new EnvText(env, "4. Quitter le jeu ?", 250, 220);
        textNomJoueur = new EnvText(env, "Choisissez un nom de joueur : ", 200, 300);
        datation = new EnvText(env, "Choisissez une date d'enregistrement pour votre partie : ", 200, 300);
        textMenuPrincipal1 = new EnvText(env, "1. Charger un profil de joueur existant ?", 250, 280);
        textMenuPrincipal2 = new EnvText(env, "2. Créer un nouveau joueur ?", 250, 260);
        textMenuPrincipal3 = new EnvText(env, "3. Sortir du jeu ?", 250, 240);
        
        textNiveauU  = new EnvText(env, "Niveau ?", 250, 300);
        textNiveau1 = new EnvText(env, "1 - facile (3 lettres) ", 260, 280);
        textNiveau2 = new EnvText(env, "2 - uhm facile+ (4 lettres)", 270, 260);
        textNiveau3 = new EnvText(env, "3 - intermediaire (5 lettres)", 280, 240);
        textNiveau4 = new EnvText(env, "4 - intermediaire + (6 lettres)", 290, 220);
        textNiveau5 = new EnvText(env, "5 - Difficile (8 lettres)", 300, 200);
        text  = new EnvText(env, "", 300, 400);
        this.finGame = new EnvText(env,"Perdu !!!", 300, 400); 
        this.finGameTemps = new EnvText(env, "Perdu", 350, 400);

    }

    /**
     * Gère le menu principal
     *
     */
    public void execute() {
        MENU_VAL mainLoop;
        mainLoop = MENU_VAL.MENU_SORTIE;
        do {
            mainLoop = menuPrincipal();
        } while (mainLoop != MENU_VAL.MENU_SORTIE);
        this.env.setDisplayStr("Au revoir !", 300, 30);
        env.exit();
    }

    /**
     * Teste si une code clavier correspond bien à une lettre
     *
     * @return true si le code est une lettre
     */
    private Boolean isLetter(int codeKey) {
        return codeKey == Keyboard.KEY_A || codeKey == Keyboard.KEY_B || codeKey == Keyboard.KEY_C || codeKey == Keyboard.KEY_D || codeKey == Keyboard.KEY_E
                || codeKey == Keyboard.KEY_F || codeKey == Keyboard.KEY_G || codeKey == Keyboard.KEY_H || codeKey == Keyboard.KEY_I || codeKey == Keyboard.KEY_J
                || codeKey == Keyboard.KEY_K || codeKey == Keyboard.KEY_L || codeKey == Keyboard.KEY_M || codeKey == Keyboard.KEY_N || codeKey == Keyboard.KEY_O
                || codeKey == Keyboard.KEY_P || codeKey == Keyboard.KEY_Q || codeKey == Keyboard.KEY_R || codeKey == Keyboard.KEY_S || codeKey == Keyboard.KEY_T
                || codeKey == Keyboard.KEY_U || codeKey == Keyboard.KEY_V || codeKey == Keyboard.KEY_W || codeKey == Keyboard.KEY_X || codeKey == Keyboard.KEY_Y
                || codeKey == Keyboard.KEY_Z || codeKey == Keyboard.KEY_0 || codeKey == Keyboard.KEY_1 || codeKey == Keyboard.KEY_2 || codeKey == Keyboard.KEY_3
                || codeKey == Keyboard.KEY_4 || codeKey == Keyboard.KEY_5 || codeKey == Keyboard.KEY_6 || codeKey == Keyboard.KEY_7 || codeKey == Keyboard.KEY_8 
                || codeKey == Keyboard.KEY_9 || codeKey == Keyboard.KEY_BACKSLASH
                ;
    }

    /**
     * Récupère une lettre à partir d'un code clavier
     *
     * @return une lettre au format String
     */
    private String getLetter(int letterKeyCode) {
        Boolean shift = false;
        if (this.env.getKeyDown(Keyboard.KEY_LSHIFT) || this.env.getKeyDown(Keyboard.KEY_RSHIFT)) {
            shift = true;
        }
        env.advanceOneFrame();
        String letterStr = "";
        if ((letterKeyCode == Keyboard.KEY_SUBTRACT || letterKeyCode == Keyboard.KEY_MINUS)) {
            letterStr = "-";
        } else {
            letterStr = Keyboard.getKeyName(letterKeyCode);
        }
        if (shift) {
            return letterStr.toUpperCase();
        }
        return letterStr.toLowerCase();
    }
    
    private String getChiffre(int letterKeyCode) {
        Boolean shift = false;
        if (this.env.getKeyDown(Keyboard.KEY_LSHIFT) || this.env.getKeyDown(Keyboard.KEY_RSHIFT)) {
            shift = true;
        }
        env.advanceOneFrame();
        String letterStr = "";
        if ((letterKeyCode == Keyboard.KEY_SUBTRACT || letterKeyCode == Keyboard.KEY_MINUS)) {
            letterStr = "/";
        } else {
            letterStr = Keyboard.getKeyName(letterKeyCode);
        }
        if (shift) {
            return letterStr.toUpperCase();
        }
        return letterStr.toLowerCase();
    }


    /**
     * Permet de saisir le nom d'un joueur et de l'affiche à l'écran durant la saisie
     *
     * @return le nom du joueur au format String
     */
    private String getNomJoueur() {
        textNomJoueur.modify("Choisissez un nom de joueur : ");
        int touche = 0;
        String nomJoueur = "";
        while (!(nomJoueur.length() > 0 && touche == Keyboard.KEY_RETURN)) {
            touche = 0;
            while (!isLetter(touche) && touche != Keyboard.KEY_MINUS && touche != Keyboard.KEY_SUBTRACT && touche != Keyboard.KEY_RETURN) {
                touche = env.getKey();
                env.advanceOneFrame();
            }
            if (touche != Keyboard.KEY_RETURN) {
                if ((touche == Keyboard.KEY_SUBTRACT || touche == Keyboard.KEY_MINUS) && nomJoueur.length() > 0) {
                    nomJoueur += "-";
                } else {
                    nomJoueur += getLetter(touche);
                }
                textNomJoueur.modify("Choisissez un nom de joueur : " + nomJoueur);
            }
        }
        textNomJoueur.erase();
        return nomJoueur;
    }
    
    private String getDate() {
        datation.modify("Choisissez une date à enregistrer/charger : ");
        int touche = 0;
        String date = "";
        while (!(date.length() > 0 && touche == Keyboard.KEY_RETURN)) {
            touche = 0;
            while (!isLetter(touche) && touche != Keyboard.KEY_MINUS && touche != Keyboard.KEY_SUBTRACT && touche != Keyboard.KEY_RETURN) {
                touche = env.getKey();
                env.advanceOneFrame();
            }
            if (touche != Keyboard.KEY_RETURN) {
                if ((touche == Keyboard.KEY_SUBTRACT || touche == Keyboard.KEY_MINUS) && date.length() > 0) {
                    date += "-";
                } else {
                    date += getChiffre(touche);
                }
                datation.modify("Choisissez une date à enregistrer/charger : " + date);
            }
        }
        datation.erase();
        return date;
    }
    

    
    /**
     * Menu principal
     *
     * @return le choix du joueur
     */
    private MENU_VAL menuPrincipal() {

        MENU_VAL choix = MENU_VAL.MENU_CONTINUE;
        String nomJoueur;

        // restaure la room du menu
        env.setRoom(menuRoom);

        // affiche le menu principal
        textMenuQuestion.display();
        textMenuPrincipal1.display();
        textMenuPrincipal2.display();
        textMenuPrincipal3.display();

        // vérifie qu'une touche 1, 2 ou 3 est pressée
        int touche = 0;
        while (!(touche == Keyboard.KEY_1 || touche == Keyboard.KEY_2 || touche == Keyboard.KEY_3)) {
            touche = env.getKey();
            env.advanceOneFrame();
        }

        // efface le menu
        textMenuQuestion.erase();
        textMenuPrincipal1.erase();
        textMenuPrincipal2.erase();
        textMenuPrincipal3.erase();

        // et décide quoi faire en fonction de la touche pressée
        switch (touche) {
            // -------------------------------------
            // Touche 1 : Charger un profil existant
            // -------------------------------------
            case Keyboard.KEY_1:
                // demande le nom du joueur existant
                nomJoueur = getNomJoueur();
                // charge le profil de ce joueur si possible
                if (profil.charge(nomJoueur)) {
                    // lance le menu de jeu et récupère le choix à la sortie de ce menu de jeu
                    choix = menuJeu();
                } else {
                    // sinon continue (et boucle dans ce menu)
                    choix = MENU_VAL.MENU_CONTINUE;
                }
                break;

            // -------------------------------------
            // Touche 2 : Créer un nouveau joueur
            // -------------------------------------
            case Keyboard.KEY_2:
                // demande le nom du nouveau joueur
                nomJoueur = getNomJoueur();
                
                // crée un profil avec le nom d'un nouveau joueur
                profil.setDateNaissance("01/05/1996");
                profil = new Profil(nomJoueur, profil.getDateNaissance());
                // lance le menu de jeu et récupère le choix à la sortie de ce menu de jeu
                choix = menuJeu();
                break;

            // -------------------------------------
            // Touche 3 : Sortir du jeu
            // -------------------------------------
            case Keyboard.KEY_3:
                // le choix est de sortir du jeu (quitter l'application)
                choix = MENU_VAL.MENU_SORTIE;
        }
        return choix;
    }

    
    /**
     * Menu de jeu
     *
     * @return le choix du joueur une fois la partie terminée
     */
    private MENU_VAL menuJeu() {

        MENU_VAL playTheGame;
        playTheGame = MENU_VAL.MENU_JOUE;
        Partie partie;
        do {
            // restaure la room du menu
            env.setRoom(menuRoom);

            // affiche le menu de jeu
            textMenuQuestion.display();
            textMenuJeu1.display();
            textMenuJeu2.display();
            textMenuJeu3.display();
            textMenuJeu4.display();

            // vérifie qu'une touche 1, 2, 3 ou 4 est pressée
            int touche = 0;
            while (!(touche == Keyboard.KEY_1 || touche == Keyboard.KEY_2 || touche == Keyboard.KEY_3 || touche == Keyboard.KEY_4)) {
                touche = env.getKey();
                env.advanceOneFrame();
            }

            // efface le menu
            textMenuQuestion.erase();
            textMenuJeu1.erase();
            textMenuJeu2.erase();
            textMenuJeu3.erase();
            textMenuJeu4.erase();

            // restaure la room du jeu
            env.setRoom(room);

            // et décide quoi faire en fonction de la touche pressée
            switch (touche) {
                // -----------------------------------------
                // Touche 1 : Commencer une nouvelle partie
                // -----------------------------------------                
                case Keyboard.KEY_1: // choisi un niveau et charge un mot depuis le dico
                    
                    env.setRoom(menuRoom);
                    this.textNiveauU.display();
                    this.textNiveau1.display();
                    this.textNiveau2.display();
                    this.textNiveau3.display();
                    this.textNiveau4.display();
                    this.textNiveau5.display();
                    int toucheF = 0;
                    
                    while (!(toucheF == Keyboard.KEY_1 || toucheF == Keyboard.KEY_2 || toucheF == Keyboard.KEY_3 || toucheF == Keyboard.KEY_4 || toucheF == Keyboard.KEY_5 )) {
                    toucheF = env.getKey();
                        env.advanceOneFrame();
                    }
                    
                    this.textNiveauU.erase();
                    this.textNiveau1.erase();
                    this.textNiveau2.erase();
                    this.textNiveau3.erase();
                    this.textNiveau4.erase();
                    this.textNiveau5.erase();
                    
                        int niv = 0;
                        switch(toucheF){
                            case Keyboard.KEY_1:
                                niv = 1;
                                break;
                            case Keyboard.KEY_2:
                                niv = 2;
                                break;
                            case Keyboard.KEY_3:
                                niv = 3;
                                break; 
                            case Keyboard.KEY_4:
                                niv = 4;
                                break;
                            case Keyboard.KEY_5:
                                niv = 5;
                                break;
                        }
                  
                   
                 
                    try {
                        dico.lireDictionnaire();
                    } catch (IOException ex) {
                        Logger.getLogger(Jeu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                
                    
                    
                    System.out.println("niv :" + niv);
                    System.out.println("mot :" + dico.getMotDepuisListeNiveaux(niv));

                    // crée un nouvelle partie
                    
                    partie = new Partie(this.localTimer(), dico.getMotDepuisListeNiveaux(niv), niv);
                    System.out.println("dico :" + partie.getMot());
                                        
                                        
                    env.setRoom(menuRoom);
                    this.text.modify(partie.getMot());
                    env.advanceOneFrame();
                    chrono.start();
                    do{}while(chrono.tempsSeconde()== 0);// le temps d'affichage est de 3s pour la lettre a trouver
                    chrono.stop();
                    text.erase();
                    env.advanceOneFrame();
                    
                    // joue
                    joue(partie);
                    // enregistre la partie dans le profil --> enregistre le profil
                    profil.sauvegarder("src/XML/profil.xml", partie);
                    // .......... profil .........
                    playTheGame = MENU_VAL.MENU_JOUE;
                    break;

                // -----------------------------------------
                // Touche 2 : Charger une partie existante
                // -----------------------------------------                
                case Keyboard.KEY_2: // charge une partie existante
                    // demander de fournir une date
                    // tenter de trouver une partie à cette date
                    //System.out.println("nom du joueur : " + profil.getNom());
                    
                    partie = new Partie("another day", "Default", 1);
                    if(partie.charger_partie(profil.getNom(),"10/12/2013")){
                        
                        partie.charger_partie(profil.getNom(),"10/12/2013");
                        partie = new Partie("Aujourd'hui", partie.getMot(), 1);
                    }
                    // Si partie trouvée, recupère le mot de la partie existante, sinon ???
                    else{
                        return null;
                    }
                    // joue
                    joue(partie);
                    // enregistre la partie dans le profil --> enregistre le profil
                    profil.sauvegarder("src/XML/profil.xml", partie);
                    
                    playTheGame = MENU_VAL.MENU_JOUE;
                    break;

                // -----------------------------------------
                // Touche 3 : Sortie de ce jeu
                // -----------------------------------------                
                case Keyboard.KEY_3:
                    playTheGame = MENU_VAL.MENU_CONTINUE;
                    break;

                // -----------------------------------------
                // Touche 4 : Quitter le jeu
                // -----------------------------------------                
                case Keyboard.KEY_4:
                    playTheGame = MENU_VAL.MENU_SORTIE;
            }
        } while (playTheGame == MENU_VAL.MENU_JOUE);
        return playTheGame;
    }

    
    public void joue(Partie partie){
 
        // TEMPORAIRE : on règle la room de l'environnement. Ceci sera à enlever lorsque vous ajouterez les menus.
        env.setRoom(room);
        tux = new Tux(env, room);
        env.addObject(tux);
        String motAAfficher;

        /*
         lettres.add(new Letter('s', 0.3, 0.3));
         lettres.add(new Letter('a', 4, 4));
         lettres.add(new Letter('l', 6, 6));
         lettres.add(new Letter('u', 5, 3));
         lettres.add(new Letter('t', 7, 7));
        */
        int Min = 1;
        int Max = 7;
        
        
        // Choix du niveau 
        motAAfficher = partie.getMot();
        
        
        for(int i=0; i<= motAAfficher.length()-1; i++){
            int nb1 = Min + (int)(Math.random() * ((Max - Min) + 1));
            int nb2 = Min + (int)(Math.random() * ((Max - Min) + 1));
            //System.out.println(nb1 +" "+ nb2);

            lettres.add(new Letter(motAAfficher.charAt(i), nb1, nb2));
            System.out.println(lettres.get(i));
            this.distance(lettres.get(i));
            
        }

        // Ici, on peut initialiser des valeurs pour une nouvelle partie
       
        for(Letter l : lettres){
            env.addObject(l);
        }
        
        démarrePartie(partie);

        // Boucle de jeu
        Boolean finished;
        finished = false;
        while (!finished) {

            // Contrôles globaux du jeu (sortie, ...)
            //1 is for escape key
            if (env.getKey() == 1) {
                finished = true;
            }
 
            // Contrôles des déplacements de Tux (gauche, droite, ...)
            // ... (sera complété plus tard) ...
            tux.deplace();
            
                       
            
            // Ici, on applique les regles
            appliqueRegles(partie);
 
            // Fait avancer le moteur de jeu (mise à jour de l'affichage, de l'écoute des événements clavier...)
            env.advanceOneFrame();
        }
 
        // Ici on peut calculer des valeurs lorsque la partie est terminée
        terminePartie(partie);
        

    }  

    public Profil getProfil() {
        return profil;
    }

    public ArrayList<Letter> getLettres() {
        return lettres;
    }
    
    public double distance(Letter letter){
        double x1 = this.tux.getX();
        System.out.println("x1 : "+x1);
        double y1 = this.tux.getZ();
        System.out.println("y1 : "+y1);
        
        double x2 =letter.getX();
        System.out.println("x2 : "+x2);

        double y2 =letter.getZ();
        System.out.println("y2 : "+y2);

        double distance = Math.sqrt((x2 - x1)*(x2 - x1) +(y2 - y1)*(y2 - y1));
        System.out.println("distance : "+distance);
        return distance;
    }
    
    public boolean collision(Letter letter){
        boolean verif = false;
        if(this.distance(letter) < (tux.getScale() + letter.getScale())){
            verif = true;
        }
        return verif;
    }
    
    public void retirerLettreTrouve(Letter lettre){
        env.removeObject(lettre);
    }
    
    protected abstract void démarrePartie(Partie partie);

    protected abstract void appliqueRegles(Partie partie);

    protected abstract void terminePartie(Partie partie);
    
    public String localTimer(){
        return LocalDate.now().toString();
    }
    
    public void template(String textAafficher){
        env.setRoom(menuRoom);
        this.text.modify(textAafficher);
        env.advanceOneFrame();
        text.erase();
        env.advanceOneFrame();
        MENU_VAL playTheGame = MENU_VAL.MENU_JOUE;
    }
}
