/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import game.Jeu;
import static game.Profil.profileDateToXmlDate;
import static game.Profil.xmlDateToProfileDate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import xml.XMLUtil;

/**
 *
 * @author Vafing
 */
public class Partie {
    
    
    private String date;// format(yyyy-mm-dd)
    private String mot;
    private int niveau;
    private int trouve;
    private int temps;
    private String MotTrouve;

    public Partie(String date, String mot, int niveau) {
        this.date = date;
        this.mot = mot;
        this.niveau = niveau;
    }
    
    public Partie(Element partieElt){
        

        this.mot = partieElt.getElementsByTagName("ns1:mot").item(0).getTextContent();
        //System.out.println(mot);
        this.niveau = Integer.parseInt(partieElt.getElementsByTagName("ns1:mot").item(0).getAttributes().getNamedItem("ns1:niveau").getTextContent());
        //System.out.println(niveau);
        this.date = partieElt.getAttribute("ns1:date");
        //System.out.println(date);
        
       if(partieElt.getElementsByTagName("ns1:temps").getLength()!=0){
            this.temps= (int) Double.parseDouble(partieElt.getElementsByTagName("ns1:temps").item(0).getTextContent());
        }else{
            this.temps=0;
        }
        
    }
    
    public Element getPartie(Document doc){
        Element parties = null;
        Element partie = null;
        Element mot = null;
        Element temps = null;
        
        parties = doc.createElement("ns1:parties");

        partie = doc.createElement("ns1:partie");
        partie.setAttribute("ns1:date", profileDateToXmlDate(date));// format(yyyy-mm-dd)
        parties.appendChild(partie);
        
        mot = doc.createElement("ns1:mot");
        mot.setTextContent(this.mot);
        mot.setAttribute("ns1:niveau", niveau + "");
        partie.appendChild(mot);
        
        if(this.temps >= 1){
            temps = doc.createElement("ns1:temps");
            temps.setTextContent(this.temps+"");
            partie.appendChild(temps);
        }
        parties = doc.createElement("ns1:parties");
        
        
        return partie;
    }

    public String getMot() {
        return mot;
    }
    
    public void setTrouve(int nbLettresRestantes){
        this.trouve = nbLettresRestantes;
    }

    public String getMotTrouve() {
        return MotTrouve;
    }
    
    public void setMotTrouve(String mot){
        this.MotTrouve = mot;
    } 
    
    public void setTemps(int temps) {
        this.temps = temps;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public int getNiveau() {
        return niveau;
    }

    public int getTemps() {
        return temps;
    }

    public void setMot(String mot) {
        this.mot = mot;
    }

    public void setNiveau(int niveau) {
        this.niveau = niveau;
    }
    
    
    public String toString(){
        return "Temps de la partie :" + temps + " secondes" + " Pourcentage de lettres trouvé : " + trouve;
    }
    
    public boolean charger_partie(String nomJoueur, String date){
        
        try {
             Document doc;
            doc = XMLUtil.DocumentFactory.fromFile("src/xml/profil.xml");
                       
            int i = 0;
            int place = 0;
            int place_2 = 0;
            Node n = null;
            NodeList partie = doc.getElementsByTagName("ns1:partie");
            NodeList nom = doc.getElementsByTagName("ns1:nom");
            Node mot = null;
            Node temps = null;
            int punk = 0;
            System.out.println("nom longueur :"+ nom.getLength() +"\npartie longueur:" + partie.getLength());
            
            for(i = 0; i < nom.getLength(); i++){
                n = nom.item(i);
                //if(nomJoueur == n.getTextContent())
                if(nomJoueur == null ? n.getTextContent() == null : nomJoueur.equals(n.getTextContent())){
                    place = i;
                }
            }
            System.out.println("place : " + place);
            // recuperation des parties du joueur nomJoueur
            NodeList parties = doc.getElementsByTagName("ns1:parties");
            //System.out.println("parties : "+parties.item(place).getNodeName());
            Node partys = parties.item(place);
            //System.out.println("partie : "+parties.item(place).getChildNodes().item(1).getNodeName() +" "+ parties.item(place).getChildNodes().item(3).getNodeName());
            //verfie laquelle des parties on veut a la position place_2
            //System.out.println("length partie :" + partys.getChildNodes().getLength());
            for(i = 0; i< partys.getChildNodes().getLength(); i++){
                //System.out.println(i);
                if(i == 1 || i == 3 || i == 5){
                    Element e = (Element) partys.getChildNodes().item(i);  
                    //System.out.println(e.getAttribute("date : ").toString() + " et parametre :"+ date);
                    if(e.getAttribute("date").equals(profileDateToXmlDate(date))){
                        //System.out.println(n.getChildNodes().item(0)); 
                        place_2 = i;
                        //System.out.println("place 2: " + place_2);
                    }
                }
            }
            //System.out.println("place 2 :"+place_2);
            //recuperation de la partie numero place_2
            Node recup = partys.getChildNodes().item(place_2);
            //System.out.println("recup : " + recup.getNodeName());
            //System.out.println(recup.getNodeName());
            mot = recup.getChildNodes().item(3);
            //System.out.println(mot);
            System.out.println("mot : " + mot.getTextContent());
            this.mot = mot.getTextContent();
            temps = recup.getChildNodes().item(1);
            //System.out.println("mot : " + temps);
            if(temps != null){
                this.temps = (int) Double.parseDouble(temps.getTextContent());
                System.out.println("temps  : " + this.temps);
            }else{
                this.temps = 0;
                //System.out.println("temps : " + this.temps);
            }
            Chronometre chrono;
            chrono = new Chronometre(this.temps);
            
            // verifier le nom est a quelle position et choisir a quelle position se trouve la party
        } catch (Exception ex) {
            Logger.getLogger(Partie.class.getName()).log(Level.SEVERE, null, ex);
        }
            
       
        return true;
    }
    
    public boolean compareLettre(String s, String v){
        int j = 0;
        for(int i = 0; i< s.length(); i++){
            if(s.charAt(i) != v.charAt(i)){
               return false;
            }
        }
        return true;
    }
    
}
