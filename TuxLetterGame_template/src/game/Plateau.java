/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xml.XMLUtil;

/**
 *
 * @author Vafing
 */
public class Plateau {
    private int depth;
    private int height;
    private int width;
    private String textureBottom;
    private String textureEast;
    private String textureWest;
    private String textureNorth;
    
    public Plateau() throws SAXException, IOException{
        
            DOMParser parser = new DOMParser() ;
            parser.parse("src/XML/plateau.xml");
            Document doc = parser.getDocument();
            NodeList textureBottom = doc.getElementsByTagName("textureBottom");
            NodeList textureEast = doc.getElementsByTagName("textureEast");
            NodeList textureWest = doc.getElementsByTagName("textureWest");
            NodeList textureNorth = doc.getElementsByTagName("textureNorth");

            this.textureBottom = textureBottom.item(0).getChildNodes().item(1).getTextContent();
            this.textureEast = textureEast.item(0).getChildNodes().item(1).getTextContent();
            this.textureWest = textureWest.item(0).getChildNodes().item(1).getTextContent();
            this.textureNorth = textureNorth.item(0).getChildNodes().item(1).getTextContent();
            
            System.out.println(this.textureBottom + "\n" + this.textureEast + "\n" + this.textureWest + "\n" +this.textureNorth);
            
    }

    public int getDepth() {
        try {
            Document doc = XMLUtil.DocumentFactory.fromFile("src/XML/plateau.xml");
            this.depth = Integer.parseInt(doc.getElementsByTagName("depth").item(0).getTextContent());
        } catch (Exception ex) {
            Logger.getLogger(Plateau.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(this.depth);
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getHeight() {
        try {
            Document doc = XMLUtil.DocumentFactory.fromFile("src/XML/plateau.xml");
            this.height = Integer.parseInt(doc.getElementsByTagName("ns1:height").item(0).getTextContent());
        } catch (Exception ex) {
            Logger.getLogger(Plateau.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(this.height);
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        try {
            Document doc = XMLUtil.DocumentFactory.fromFile("src/XML/plateau.xml");
            this.width = Integer.parseInt(doc.getElementsByTagName("ns1:width").item(0).getTextContent());
        } catch (Exception ex) {
            Logger.getLogger(Plateau.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(this.width);
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getTextureBottom() {
        return textureBottom;
    }

    public void setTextureBottom(String textureBottom) {
        this.textureBottom = textureBottom;
    }

    public String getTextureEast() {
        return textureEast;
    }

    public void setTextureEast(String textureEast) {
        this.textureEast = textureEast;
    }

    public String getTextureWest() {
        return textureWest;
    }

    public void setTextureWest(String textureWest) {
        this.textureWest = textureWest;
    }

    public String getTextureNorth() {
        return textureNorth;
    }

    public void setTextureNorth(String textureNorth) {
        this.textureNorth = textureNorth;
    }
    
    
}
